# coding: utf-8

"""
    * Nome: Angelo Victor Kraemer Foletto
    * Numero USP: 12620258
    * Código do Curso: SCC5830 - Image Processing (2021) / 1
    * Docente: Moacir Antonelli Ponti
    * Assignment 5: Image Descriptors
    * Data 04/07/2021
    * Create: 30/06/2021
    * Update: 04//2021
    *
    * https://gitlab.com/scc5830-image-processing/04_image_restoration
"""

## Imports
import numpy as np
import imageio as imgio
import math
from scipy import linalg, ndimage


class Module05:

    def __init__(self, img_01, img_02, quant_b):
        """

        :param img_01: String
        :param img_02: String
        :param quant_b: integer
        """
        self.img_01 = imgio.imread(img_01)
        self.img_02 = imgio.imread(img_02)
        self.quant_b = quant_b

    def _normalize(self, values, format):
        imin = np.min(values)
        swap = (values - imin) / (np.max(values) - imin)
        return (swap * format).astype(np.int16)

    def _quantisation(self, file, b):
        """
        Transform the images to black&white and quantise the image.

        :param file: obj image
        :return: obj image (black&white)
        """
        # Final variables
        R = 0.299
        G = 0.587
        B = 0.114

        img_size = file.shape[0:2]
        img_new = self._npZeros(img_size)
        bitwise = self._bitwise(b)

        for i in range(img_size[0]):
            for j in range(img_size[1]):  # (R, G, B) <-> (file[i, j][0], file[i, j][1], file[i, j][2])
                swap = ((R * file[i, j][0]) + (G * file[i, j][1]) + (B * file[i, j][2])).astype(np.int32)
                img_new[i, j] = swap >> bitwise

        return img_new

    def _bitwise(self, b):
        """
        Right bit shift

        :param b: integer
        :return: integer
        """
        return 8 - b

    def _npZeros(self, shape, dtype=np.int32):
        """
        Create a np.zeros array

        :param shape: list
        :param dtype: np.type
        :return: list (np.zeros)
        """
        return np.zeros(shape, dtype=dtype)

    def _accumulated(self, matrix):
        """
        Accumulated values.

        :param matrix: list()
        :return: list()
        """
        for i in range(1, matrix.shape[0]):
            matrix[i] += matrix[i - 1]
        return matrix

    def _histogram(self, img):
        """
        Find histogram accumulated in the image.

        :param img: list()
        :return: list()
        """
        matrix = self._npZeros(img.shape[0:2])
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
                matrix[int(img[i, j])] += 1
        return self._accumulated(matrix)

    def _normalisedHistogram(self, img):
        """

        :param img: list()
        :return: list()
        """
        histogram = self._histogram(img)
        normalise_histogram = histogram / np.sum(histogram)
        normalise_histogram = self._normalize(normalise_histogram, 255)
        return self._haralickTexture(normalise_histogram)

    def _haralickTexture(self, img):
        """

        :param img: list()
        :return: list()
        """
        size = img.shape[0]
        matrix = self._npZeros((size, size))
        total_sum = 0

        for i in range(size - 1):
            for j in range(size - 1):
                matrix[int(img[i, j]), int(img[i + 1, j + 1])] += 1

        for i in range(size):
            for j in range(size):
               total_sum += matrix[i, j]

        return matrix / total_sum

    def _energy(self, img):
        """
        Calc of energy at image.

        :param img: list()
        :return: list()
        """
        pow = 2
        size = img.shape[0]
        for i in range(size):
            for j in range(size):
                img[i, j] = math.pow(img[i, j], pow)
        return img

    def _entropy(self, img):
        """
        Calc of entropy at image.

        :param img: list()
        :return: list()
        """
        size = img.shape[0]
        error = 0.001
        for i in range(size):
            for j in range(size):
                img[i, j] = img[i, j] * math.log(img[i, j] + error)
        return img * (-1)

    def _contrast(self, img):
        """
        Calc contrast of image.

        :param img: list()
        :return: list()
        """
        size = img.shape[0]
        pow = 2
        for i in range(size):
            for j in range(size):
                img[i, j] = math.pow((i - j), pow) * img[i, j]
        img *= 1 / math.pow(size, pow)
        return img

    def _meansRow(self, img):
        """
        Calc means of rows, represented with Ui.

        :param img: list()
        :return: list()
        """
        for i in range(img.shape[0]):
            img[i, :] *= i
        return img

    def _meansCol(self, img):
        """
        Calc means of collum, represented with Uj

        :param img: list()
        :return: list()
        """
        for j in range(img.shape[0]):
            img[:, j] *= j
        return img

    def _standartDesviationRow(self, img, meanUI):
        """
        Calc at rows of standart desviation, represented with σi

        :param img: list()
        :param meanUI: list()
        :return: list()
        """
        pow = 2
        for i in range(img.shape[0]):
            img[i, :] *= math.pow(i - meanUI[i, :].sum(), pow)
        return img

    def _standartDesviationCol(self, img, meanUJ):
        """
        Calc at collum of standart desviation, represented with σj

        :param img: list()
        :param meanUJ: list()
        :return: list()
        """
        pow = 2
        for j in range(img.shape[0]):
            img[:, j] *= math.pow(j - meanUJ[:, j].sum(), pow)
        return img

    def _correlation(self, img):
        """
        Calc of correlation

        :param img: list()
        :return: list()
        """
        ui = self._meansRow(img)
        uj = self._meansCol(img)
        oi = self._standartDesviationRow(img, ui)
        oj = self._standartDesviationCol(img, uj)
        ui_sum = ui.sum()
        uj_sum = uj.sum()

        size = img.shape[0]
        for i in range(size):
            for j in range(size):
                img[i, j] = i * j * img[i, j] - ui_sum * uj_sum
        img /= oi.sum() * oj.sum()

        return self._normalize(img * (-1), 255)

    def _homogeneity(self, img):
        """
        Calc of homogeneity

        :param img: list()
        :return: list()
        """
        size = img.shape[0]
        for i in range(size):
            for j in range(size):
                img[i, j] = img[i, j] / (1 + abs(i - j))
        return img

    def _computingDescriptors(self, img):
        """
        Union and apply energy, entropy, contrast, correlation
        and homogeneity.

        :param img: list()
        :return: list()
        """
        energy = self._energy(img)
        entropy = self._entropy(energy)
        contrast = self._contrast(entropy)
        correlation = self._correlation(contrast)
        homogeneity = self._homogeneity(correlation)
        return homogeneity

    def _orientedGradients(self, img):
        """"""
        pow = 2
        bins = np.arange(20, 100, 20)

        gx = np.power(ndimage.convolve(img, [[-1, -2, -1], [0, 0, 0], [1, 2, 1]]), pow)
        gy = np.power(ndimage.convolve(img, [[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]]), pow)
        sqrt_g = np.sqrt(gx + gy)

        magnitude = sqrt_g / gx.sum() * gy.sum() * sqrt_g
        magnitude_bins = np.digitize(magnitude, bins)

        angule = np.power(np.tan(gy / gx), -1)
        angule += np.pi / 2
        angule = np.degrees(angule)
        angule_bins = np.digitize(angule, bins)

        norml_magnitude_bins = magnitude_bins / linalg.norm(magnitude_bins)

        np.seterr(all='ignore')

        return norml_magnitude_bins

    def _d(self, img, bins):
        """
        Builb a image 01 and 02, return all methods concatenate.

        :param img: list()
        :param bins: int
        :return: list()
        """
        quantisation = self._quantisation(img, bins)
        dc_normalise_histogram = self._normalisedHistogram(quantisation)
        dt_computing_descriptros = self._computingDescriptors(dc_normalise_histogram)  ## Computing Descriptors
        dg_oriented_gradients = self._orientedGradients(dt_computing_descriptros)
        return np.concatenate((dc_normalise_histogram, dt_computing_descriptros, dg_oriented_gradients))

    def _compare(self, img1, img2):
        """"""

    def main(self):
        """
        The main

        :return:
        """
        # Image 02
        build_d = self._d(self.img_02, self.quant_b)

        # Image 01 - small
        build_di = self._d(self.img_01, self.quant_b)

        print(build_d)
        print(build_di)



class Debug:
    _path = 'files/ImagesForDebugging/'

    def img01(self):
        self.img_01 = imgio.imread('{}wally.png'.format(self._path))
        self.img_02 = imgio.imread('{}wheres_wally.png'.format(self._path))
        self.quant_b = 3

        a = Module05('{}wally.png'.format(self._path), '{}wheres_wally.png'.format(self._path), self.quant_b)
        a.main()

        # self.plot(self.img_02, 12, 12)
        # self.plot(a.main(), 12, 12)

    def plot(self, search_img, j, i):
        import matplotlib.pyplot as plt
        import matplotlib.patches as patches
        fig, ax = plt.subplots()
        ax.imshow(search_img)
        rect = patches.Rectangle((j * 16, i * 16), 32, 32,
                                 linewidth=1, edgecolor='r',
                                 facecolor='none')
        ax.add_patch(rect)
        plt.show()

    def main(self):
        self.img01()


if __name__ == '__main__':
    img_01 = str()  # Image name to find
    img_02 = str()  # Largage image name
    quant_b = int()  # Quantisation parameter 'b'

    module = Module05(img_01, img_02, quant_b)
    rmse = module.main()
    print('%.4f' % round(rmse, 4))

    ### -- deploy - debug -- ###
    #### Start

    # dbug = Debug()
    # dbug.main()

    ## End
